<?php
// print_r($_POST); response/show data ->array
require 'config.php';
if(isset($_POST['action']) && $_POST['action']=='register'){
   $fname=check_input($_POST['fname']);
   $lname=check_input($_POST['lname']);
   $remail=check_input($_POST['remail']);
   $pass=check_input($_POST['pass']);
   $cpass=check_input($_POST['cpass']);
   $pass=sha1($pass);
   $cpass=sha1($cpass);
   $created=date('Y-m-d');


   if ($pass!=$cpass) {
   	echo "Password did not matched!";
   	exit();
   }
   else{
   	$sql=$conn->prepare("SELECT email,fname FROM users WHERE email=?");
   	$sql->bind_param("s",$remail);
   	$sql->execute();
   	$result=$sql->get_result();
   	$row=$result->fetch_array(MYSQLI_ASSOC);
   	if($row['email']==$remail){
   		echo "Email is already register try different!";
   	}

   	else{

         $stmt=$conn->prepare("INSERT INTO users(fname,lname,email,pass,created) VALUES (?,?,?,?,?)");
         $stmt->bind_param("sssss",$fname,$lname,$remail,$pass,$created);
         if($stmt->execute()){
         	echo "registered Successfully. Login Now!";
         }
         else{
         	echo "Something went wrong. Please try again!";
         }
   	}
   }
}

if(isset($_POST['action'])&&($_POST['action'])=='login'){
	session_start();
	$email=$_POST['email']; //ชื่อเหมือนกับnameที่ตั้งไว้ ตรงส่วนของlogin form หน้าindex.php
	$password=sha1($_POST['password']);
	$stmt_l=$conn->prepare("SELECT * FROM users WHERE email=? AND pass=?");
	$stmt_l->bind_param("ss",$email,$password);
	$stmt_l->execute();
	$e_mail=$stmt_l->fetch();
	if($e_mail!=null){
		$_SESSION['email']=$email;
		echo "ok";
		if (!empty($_POST['rem'])) {
			setcookie("email",$_POST['email'],time()+(10*365*24*60*60));
			setcookie("password",$_POST['password'],time()+(10*365*24*60*60));
		}
		else{
			if (isset($_COOKIE['email'])) {
				setcookie("email","");
			}
			if (isset($_COOKIE['password'])) {
				setcookie("password","");
			}
		}
	}
	else{
		echo "Login Failed! Check your email and password";
	}
}

if(isset($_POST['action'])&&($_POST['action'])=='forgot'){
	$femail=$_POST['femail'];

	$stmt_p=$conn->prepare("SELECT id FROM users WHERE email=?");
	$stmt_p->bind_param("s",$femail);
	$stmt_p->execute();
	$res=$stmt_p->get_result();

	if($res->num_rows>0){
		$token="qwertyuiopasdfghjklzxcvbnm1234567890";
		$token=str_shuffle($token);
		$token=substr($token,0,10);

		$stmt_i=$conn->prepare("UPDATE users SET token=?,tokenExpire=DATE_ADD(NOW(),INTERVAL 5 MINUTE) WHERE email=?");
		$stmt_i->bind_param("ss",$token,$femail);
		$stmt_i->execute();

		require 'phpmailer/PHPMailerAutoload.php';
		$mail= new PHPMailer;
		$mail->Host='smtp.gmail.com';
		$mail->Port=587;
		$mail->isSMTP();
		$mail->SMTPAuth=true;
		$mail->SMTPSecure='tls';

		$mail->Username='aomsin1862019@gmail.com';
		$mail->Password='190242aom';

		$mail->addAddress($femail);
		$mail->setFrom('aomsin1862019@gmail.com','Aomsin 186');
		$mail->Subject='Reset Password';
		$mail->isHTML(true);

		$mail->Body="<h3>Click the below link to reset your password.</h3><br><a href='http://localhost:81/comp_login/resetPassword.php?email=$femail&token=$token'>http://localhost:81/comp_login/resetPassword.php?email=$femail&token=$token</a><br><h3>Regards<br>Aomsin 186</h3>";

		if($mail->send()){
			echo 'We have send you the reset link in your email ID,please check your email.';
		}
		else{
			echo 'Something went wrong please try again later.';
		}

	}
}
function check_input($data){
	$data=trim($data);
	$data=stripcslashes($data);
	$data=htmlspecialchars($data);
	return $data;
}
?>