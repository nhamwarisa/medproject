<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Sahil Kumar">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Add Request</title>
	 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <style type="text/css">
      #second,#third,#result{
      display:none;}
    </style>

</head>
<body background="pic/m01.jpg" bgproperties=fixed>
	<nav class="navbar navbar-expand-sm bg-success navbar-dark">
  <!-- Brand -->
  <!-- <a class="navbar-brand " href="#">MIS</a> -->
  <nav class="navbar navbar-light bg-faded">
  <a class="navbar-brand" href="#">
    <img src="pic/Logo_MED_TH.png" width="80" height="80" alt="">
  </a>
</nav>


<p class="navbar navbar-light ml-auto"></p>
<nav class="text-center text-light" >
  <a class="navbar-item ">
    งานเทคโนโลยีสารสนเทศคณะแพทยศาตร์
    <br>
    __________________________________________
    <br>
    มหาวิทยาลัยเชียงใหม่
  </a>
</nav>

  <!-- Links -->
  <ul class="navbar-nav ml-auto"> <!-- ml-auto ขยับออกจากฝั่งซ้ายเป็นสัดส่วนอัตโนมัติ -->
   <!--  <li class="nav-item">
      <a class="nav-link" href="#">Services</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Blog</a>
    </li> -->

    <li class="nav-item dropdown">

   <!-- <i class="fa fa-th" style="font-size:24px;color:white"></i> -->

   <a class="nav-link dropdown-toggle" href="#" id="navbardrop1" data-toggle="dropdown">
        <img src="pic/grid-icon-63640.png" width="40" height="40" alt="">
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="user.php"><img src="pic/house.png" width="35" height="35" alt=""> Home</a>
        <a class="dropdown-item" href="add.php"><img src="pic/addn.png" width="35" height="35" alt=""> Add</a>
        <a class="dropdown-item" href="#"><img src="pic/Boss-3-icon-300x300.png" width="35" height="35" alt=""> Profile</a>
        <a class="dropdown-item" href="contact_user.php"><img src="pic/Circle-icons-contacts.svg.png" width="35" height="35" alt=""> Contact</a>
       <a class="dropdown-item" href="logout.php"><img src="pic/276363.png" width="35" height="35" alt=""> Logout</a>
      </div>
    
    </li>

    <li class="nav-item dropdown">
      <!-- <?= $email; ?> -->
        <img src="pic/Boss-3-icon-300x300.png" width="50" height="50" alt="">
    </li>
  </ul>
</nav>
&nbsp;
<div class="container">
  <div class="row justify-content-center">
    <div class="col-lg-6  bg-light p-4 rounded mt-5">
      <h5 class="text-center text-light bg-success mb-2 p-2 rounded lead" id="result">Hello</h5>
      <div class="progress mb-3" style="height: 40px;">
      <div class="progress-bar bg-danger " role="progressbar" style="width: 20%;" id="progressBar">
        <b class="lead" id="progressText">Step - 1
        </b>
        </div>
      </div>
      <form action="user.php" method="post" id="form-data">
        <div id="first">
          <h4 class="text-center bg-primary p-1 rounded text-light">Add Request</h4>&nbsp;
          <div class="form-group">
            <label >ระบุวันที่ :</label>
            <input type="date" name="bday" max="3000-12-31" min="1000-01-01" class="form-control" id="bday">
            <b class="form-text text-danger" id="dateError"></b>
          </div>
          <div class="form-group">
          <label for="department">เลือกส่วนงาน :</label>
          <select class="form-control" id="department" name="department">
          <option>งานคลัง</option>
          <option>งานซ่อมบำรุง</option>
          <option>งานเทคโนโลยีสารสนเทศ</option>
          <option>งานนโยบายและแผน</option>
          <option>งานบริการการศึกษา</option>
          <option>งานบริการวิชาการและวิเทศสัมพันธ์</option>
          <option>งานบริหารงานบุคคล</option>
          <option>งานบริหารงานวิจัย</option>
          <option>งานบริหารทั่วไป</option>
          <option>งานประกันคุณภาพการศึกษา</option>
          <option>งานประชาสัมพันธ์</option>
          <option>งานพัสดุและยานพาหนะ</option>
          <option>งานโสตทัศนศึกษา</option>
          <option>งานอาคารสถานที่</option>
          <option>งานห้องสมุด[สังกัดสำนักหอสมุด มช.]</option>
          </select>
          </div>
          <div class="form-group">
            <a href="#" class="btn btn-outline-success"  id="next-1">ถัดไป</a>
          </div>
        </div>
        <div id="second">
        <h4 class="text-center bg-primary p-1 rounded text-light">Add Request</h4>&nbsp;
        <div class="form-group">
        <label for="renum">ระบุเลขที่เอกสาร :</label>
        <input type="text" class="form-control" name="renum" id="renum" placeholder="โปรดระบุเลขที่เอกสาร">
        <b class="form-text text-danger" id="renumError"></b>
        </div>
        <div class="form-group">
        <label for="rename">ระบุเรื่องที่ร้องขอ :</label>
        <input type="text" class="form-control" name="rename" id="rename" placeholder="โปรดระบุเรื่องที่ร้องขอ">
        <b class="form-text text-danger" id="renameError"></b>
        </div>
        <div class="form-group">
        <label for="dname">ผู้ที่เรียน :</label>
        <input type="text" class="form-control" name="dname" id="dname" placeholder="โปรดระบุชื่อผู้ที่เรียน">
        <b class="form-text text-danger" id="dnameError"></b>
        </div>
        <div class="form-group">
            <a href="#" class="btn btn-outline-success"  id="prev-2">กลับ</a>
            <a href="#" class="btn btn-outline-success"  id="next-2">ถัดไป</a>
        </div>
        </div>
        <div id="third">
        <h4 class="text-center bg-primary p-1 rounded text-light">Add Request</h4>&nbsp;
        <div class="form-group">
        <label for="des">ระบุรายละเอียด :</label>
        <textarea class="form-control" rows="5" name="des" id="des"></textarea> 
        <b class="form-text text-danger" id="desError"></b><br>
        
        <div class="form-group">
          <label for="status">ระบุสถานะ :</label>
          <select class="form-control" id="status" name="status">
          <option>ปกติ</option>
          <option>ด่วน</option>
          <option>ด่วนมาก</option>
          </select>
          </div>
        
        <div class="form-group">
        <a href="#" class="btn btn-outline-success"  id="prev-3">กลับ</a>
        <input type="submit" name="submit" value="เสร็จสิ้น" id="submit" class="btn btn-success"  >
        </div>
        </div>
        </div>
      </form>
    </div>
  </div>
</div>



    
  
 

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript">
  $(document).ready(function(){

   
   $("#next-1").click(function(e){

    e.preventDefault();
    $('#dateError').html('');

    if($('#bday').val() == ''){
      $('#dateError').html('* Date is required.');
      return false;

    }
    else{
      $("#second").show();
      $("#first").hide();
      $("#progressBar").css("width","60%");
      $('#progressText').html("Step - 2");
    }
      
   });

   $("#next-2").click(function(e){
      e.preventDefault();
      $('#renumError').html('');
      $('#renameError').html('');
      $('#dnameError').html('');

      if($('#renum').val()==''){
        $('#renumError').html('* Request Number is required.');
        return false;
      }
      else if($('#renum').val().length <3){
        $('#renumError').html('* Request Number must be of more than 3 characters.');
        return false;
      }

      if($('#rename').val()==''){
        $('#renameError').html('* Request Name is required.');
        return false;
      }
      else if($('#rename').val().length <3){
        $('#renameError').html('* Request Name must be of more than 3 characters.');
        return false;
      }
      else if($('#dname').val()==''){
        $('#dnameError').html('* Name of dean/head is required.');
        return false;
      }
      else if($('#dname').val().length <3){
        $('#dnameError').html('*Name of dean/head must be of more than 3 characters.');
        return false;
      }
      else if(!isNaN($('#dname').val())){
        $('#dnameError').html('* Number are not allowed.');
        return false;}
      else{
      $("#third").show();
      $("#second").hide();
      $("#progressBar").css("width","100%");
      $('#progressText').html("Step - 3");
      }  
   });

   $('#submit').click(function(e){
     
     e.preventDefault();
     $('#desError').html('');

     if($('#des').val==''){
      $('#desError').html('* Description is require.')
     }
     else if($('#des').val().length <3){
        $('#desError').html('*Description must be of more than 5 characters.');
        return false;
      }
      else{
        $.ajax(
        {
          url:'action2.php',
          method:'post',
          data:$("#form-data").serialize()+ '&action=add',
          success:function(response){

            $("#result").show();
            $("#result").html(response);
            $("#form-data")[0].reset();
          }

        });
      }
      

   });



   $("#prev-2").click(function(){
      $("#second").hide();
      $("#first").show();
      $("#progressBar").css("width","20%");
      $('#progressText').html("Step - 1");
   });

   $("#prev-3").click(function(){
      $("#third").hide();
      $("#second").show();
      $("#progressBar").css("width","60%");
      $('#progressText').html("Step - 2");
   });

   
  });
</script>




</body>
</html>
